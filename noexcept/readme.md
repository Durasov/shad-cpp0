# noexcept

Это задача типа [crashme](https://gitlab.com/slon/shad-cpp0/blob/master/crash_readme.md).

Исходный код находится в файле `main.cpp`. Исполяемый файл получен командой

```
g++ main.cpp -o main -std=c++11
```
